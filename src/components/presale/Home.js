import React from "react";
import {Link} from 'react-router-dom';


export default function Home(props) {
    let {preSalePercentage} = props;
    return (
        <>
            {/*<section className="loader_first">
                <div className="circular-spinner"></div>
            </section>*/}
            <div id="sc-banner" className="sc_banner_V3 sc_banner_V4 banner-bg position-relative">
                <div className="container">
                    <div className="banner-content V3_BanerContent">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="sc_banner_V3_left mt-10">
                                    <h2 className="wow fadeInUp" data-wow-delay="0.4s"
                                        data-wow-duration="0.6s">Boost YOUR 🚀Crypto Projects</h2>
                                    <p className="wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.7s">The
                                        next generation gaming ecosystem for IGOs and NFT market Secure crypto
                                        solutions by blockchain technology</p>
                                    <div className="gamfi_V2_hero_Btns wow fadeInUp" data-wow-delay="0.6s"
                                         data-wow-duration="0.8s">
                                        <a href="project.html"
                                           className="readon white-btn hover-shape VIEW_Projects_Btn">
                                            <span className="btn-text">VIEW Projects</span>
                                            <span className="hover-shape1"></span>
                                            <span className="hover-shape2"></span>
                                            <span className="hover-shape3"></span>
                                        </a>
                                        <a className="readon black-shape Apply_Project_Btn" href="igo-apply.html">
                                            <span className="btn-text">Apply Project</span>
                                            <span className="hover-shape1"></span>
                                            <span className="hover-shape2"></span>
                                            <span className="hover-shape3"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="sc_banner_V3_right sc_banner_V4_right mt-70">
                                    <div className="Animetion_Avater">
                                        <img src="assets/images/icons/banner-4.png" alt="img"
                                             className="img-fluid"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="HomeV4_LiveProjectSect">
                <div className="container">
                    <div className="Project_clasic_2_Sect">
                        <div className="sec-heading mb-60">
                            <div className="sub-inner mb-15">
                                <span className="sub-title">ONGOING</span>
                                <img className="heading-left-image" src="assets/images/icons/steps.png"
                                     alt="Stepsmage"/>
                            </div>
                            <div className="liveProoject_Headings">
                                <h2 className="mb-50">LIVE PROJECTS</h2>
                                <div className="video__icon">
                                    <div className="circle--outer"></div>
                                    <div className="circle--inner"></div>
                                </div>
                            </div>
                        </div>
                        <div className="Project_clasic_2_Container HomeV3_Project_clasic_2_Container">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="Project_clasic_2_ContainerLeft">
                                        <div className="Project_clasic_2_ContainerLeftContent">
                                            <div className="Project_clasic_2_ContainerLeftImg">
                                                <img src="assets/images/project/ProjectClasicContainer3.png" alt="img"
                                                     className="img-fluid"/>
                                            </div>
                                            <div className="ProjectClasic2_Timer">
                                                <div className="price-counter">
                                                    <div className="timer timer_1">
                                                        <ul>
                                                            <li className="days">03<span>D</span></li>
                                                            <li className="hours">01<span>H</span></li>
                                                            <li className="minutes">13<span>M</span></li>
                                                            <li className="seconds">56<span>S</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="Project_clasic_2_LeftContentImg">
                                                <img src="assets/images/project/privius-image11.png" alt=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <Link to="presale">
                                        <Link to="/presale">Presale</Link>
                                        <div
                                            className="Project_clasic_2Container_RightSect V3Project_clasic_2Container_RightSect">
                                            <div className="Project_clasic_2Container_RightHeadings">
                                                <h2><a href="project-details.html">Ninja Class World</a></h2>
                                                <p>price (NOO) = 0.50 BUSD</p>
                                                <span><img src="assets/images/project/icon-2.png" alt="chain"
                                                           className="img-fluid"/></span>
                                            </div>
                                            <div className="TotalRisedProgressSect">
                                                <div className="TotalRisedProgressHeadings">
                                                    <h3>Total Raised : <span>$89,000.50 / $100,000</span></h3>
                                                    <p><span className="counter">{preSalePercentage}</span>%</p>
                                                </div>
                                                <div className="progress">
                                                    <div className="progress-bar" role="progressbar"
                                                         style={{width: preSalePercentage + "%"}}
                                                         aria-valuenow="25" aria-valuemin="0"
                                                         aria-valuemax="100">{preSalePercentage}%
                                                    </div>
                                                </div>
                                                <div className="Access_Allocation_ParticipantsSect">
                                                    <ul>
                                                        <li>
                                                            <span>Access</span>
                                                            <p>Public Access</p>
                                                        </li>
                                                        <li>
                                                            <span>Allocation</span>
                                                            <p>500 BUSD Max</p>
                                                        </li>
                                                        <li>
                                                            <span>Participants</span>
                                                            <p>5555 Max</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="Project_clasic_2ContainerSocialSect">
                                                    <ul>
                                                        <li><a href="#sec"><i className="icon-telegram"></i></a></li>
                                                        <li><a href="#sec"><i className="icon-twitter"></i></a></li>
                                                        <li><a href="#sec"><i className="icon-discord"></i></a></li>
                                                        <li><a href="#sec"><i className="icon-medium"></i></a></li>
                                                        <li><a href="#sec"><i className="icon-world"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="HomeV4_LiveProjectSect">
                <div className="container">
                    <div className="Project_clasic_2_Sect">
                        <div className="sec-heading mb-60">
                            <div className="sub-inner mb-15">
                                <span className="sub-title">ONGOING</span>
                                <img className="heading-left-image" src="assets/images/icons/steps.png"
                                     alt="Stepsmage"/>
                            </div>
                            <div className="liveProoject_Headings">
                                <h2 className="mb-50">LIVE PROJECTS</h2>
                                <div className="video__icon">
                                    <div className="circle--outer"></div>
                                    <div className="circle--inner"></div>
                                </div>
                            </div>
                        </div>
                        <div className="Project_clasic_2_Container HomeV3_Project_clasic_2_Container">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="Project_clasic_2_ContainerLeft">
                                        <div className="Project_clasic_2_ContainerLeftContent">
                                            <div className="Project_clasic_2_ContainerLeftImg">
                                                <img src="assets/images/project/ProjectClasicContainer3.png" alt="img"
                                                     className="img-fluid"/>
                                            </div>
                                            <div className="ProjectClasic2_Timer">
                                                <div className="price-counter">
                                                    <div className="timer timer_1">
                                                        <ul>
                                                            <li className="days">03<span>D</span></li>
                                                            <li className="hours">01<span>H</span></li>
                                                            <li className="minutes">13<span>M</span></li>
                                                            <li className="seconds">56<span>S</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="Project_clasic_2_LeftContentImg">
                                                <img src="assets/images/project/privius-image11.png" alt=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <Link to="stacking">
                                        <Link to="/presale">Presale</Link>
                                        <div
                                            className="Project_clasic_2Container_RightSect V3Project_clasic_2Container_RightSect">
                                            <div className="Project_clasic_2Container_RightHeadings">
                                                <h2><a href="project-details.html">Ninja Class World</a></h2>
                                                <p>price (NOO) = 0.50 BUSD</p>
                                                <span><img src="assets/images/project/icon-2.png" alt="chain"
                                                           className="img-fluid"/></span>
                                            </div>
                                            <div className="TotalRisedProgressSect">
                                                <div className="TotalRisedProgressHeadings">
                                                    <h3>Total Raised : <span>$89,000.50 / $100,000</span></h3>
                                                    <p><span className="counter">{preSalePercentage}</span>%</p>
                                                </div>
                                                <div className="progress">
                                                    <div className="progress-bar" role="progressbar"
                                                         style={{width: preSalePercentage + "%"}}
                                                         aria-valuenow="25" aria-valuemin="0"
                                                         aria-valuemax="100">{preSalePercentage}%
                                                    </div>
                                                </div>
                                                <div className="Access_Allocation_ParticipantsSect">
                                                    <ul>
                                                        <li>
                                                            <span>Access</span>
                                                            <p>Public Access</p>
                                                        </li>
                                                        <li>
                                                            <span>Allocation</span>
                                                            <p>500 BUSD Max</p>
                                                        </li>
                                                        <li>
                                                            <span>Participants</span>
                                                            <p>5555 Max</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="Project_clasic_2ContainerSocialSect">
                                                    <ul>
                                                        <li><a href="#sec"><i className="icon-telegram"></i></a></li>
                                                        <li><a href="#sec"><i className="icon-twitter"></i></a></li>
                                                        <li><a href="#sec"><i className="icon-discord"></i></a></li>
                                                        <li><a href="#sec"><i className="icon-medium"></i></a></li>
                                                        <li><a href="#sec"><i className="icon-world"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="gamfi-footer-section gamfi_Home4_footer_section">
                <div className="footer-area HomeV4_FooterArea">
                    <div className="container">
                        <div className="sec-heading text-center">
                            <div className="sub-inner mb-52 mb-mb-30">
                                <img className="heading-right-image" src="assets/images/icons/steps2.png"
                                     alt="Stepsmage"/>
                                <span className="sub-title white-color">Find us on Social</span>
                                <img className="heading-left-image" src="assets/images/icons/steps.png"
                                     alt="Stepsmage"/>
                            </div>
                        </div>
                        <div className="footer-listing text-center mb-100 md-mb-70 xs-mb-50">
                            <ul className="footer-icon-list">
                                <li><a href="#sec"><i className="icon-twitter"></i></a></li>
                                <li><a href="#sec"><i className="icon-telegram"></i></a></li>
                                <li><a href="#sec"><i className="icon-medium"></i></a></li>
                                <li><a href="#sec"><i className="icon-discord"></i></a></li>
                                <li><a href="#sec"><i className="icon-linkedin"></i></a></li>
                                <li><a href="#sec"><i className="icon-instagram"></i></a></li>
                                <li><a href="#sec"><i className="icon-facebook"></i></a></li>
                            </ul>
                        </div>
                        <div className="footer-logo text-center mb-45">
                            <img src="assets/images/logo.png" alt="Footer-logo"/>
                        </div>
                        <div className="footer-mainmenu text-center mb-20">
                            <ul>
                                <li><a href="#sec">Features</a></li>
                                <li><a href="#sec">How it works</a></li>
                                <li><a href="#sec">Token info</a></li>
                                <li><a href="#sec">About us</a></li>
                                <li><a href="#sec">Social media</a></li>
                                <li><a href="#sec">Terms of Service</a></li>
                                <li><a href="#sec">Privacy Policy</a></li>
                            </ul>
                        </div>
                        <div className="copyright-area text-center mb-0">
                            <div className="dsc mb-37 md-mb-25">Copyright © 2022. All Rights Reserved by
                                <a target="_blank" className="gafi" href="#sec">GaFi</a>
                            </div>
                        </div>
                        <div className="scrollup text-center">
                            <a href="#gamfi-header"><i className="icon-arrow_up"></i></a>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}