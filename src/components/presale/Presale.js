import React, {useEffect, useState} from "react";
import {Link} from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useWeb3React } from "@web3-react/core";
import * as contractData from '../helper/contractData';
import {parseUnits} from '@ethersproject/units';
import Button from 'react-bootstrap/Button'

export default function Presale() {
    const [isLoading, setLoading] = useState(false);
    const context = useWeb3React();
    const { account , library } = context;
    const [amount, setAmount] = useState(0);
    const [riseRocket, setRiseRocket] = useState(0);

    useEffect(() => {

    })

    const amountChangeHandle = async (e) => {
        setAmount(e.target.value);
        setRiseRocket(e.target.value * 30000);
    }

    const handleBuyToken = async (e) => {
        e.preventDefault();
        setLoading(true);

        if (account) {
            try {
                // let contract = await contractData.getContract();
                let presaleMinInvestment = await contractData.getpresaleMinInvestment();
                let presaleMaxInvestment = await contractData.getpresaleMaxInvestment();

                if (presaleMinInvestment > amount) {
                    toast.error('Enter min amount ' + presaleMinInvestment); // 0.001
                    setLoading(false);
                    return false;
                }
                //Max amount validation
                if (presaleMaxInvestment < amount) {
                    toast.error('Enter max amount ' + presaleMaxInvestment); //1
                    setLoading(false);
                    return false;
                }

                let contract = await contractData.getContract(library);
                let tx = await contract.investBNB( account ,{'from' : account ,'value' : parseUnits(amount).toString()});
                let response = await tx.wait();
                if (response) {
                    if (response.status === 1) {
                        toast.success('success ! Your Last Transaction is Successfull.');
                        setAmount(0);
                        setRiseRocket(0);
                        setLoading(false);
                    } else if (response.status === 0) {
                        toast.error('error ! Your Last Transaction is Failed.');
                        setLoading(false);
                    } else {
                        toast.error('error ! something went wrong.');
                        setLoading(false);
                    }
                } else {
                    toast.error('error ! something went wrong.');
                    setLoading(false);
                }
            } catch (err) {
                typeof err.data !== 'undefined' ? toast.error(err.data.message) : toast.error(err.message);
                setLoading(false);
            }
        } else {
            toast.error('Please Connect Wallet!');
            setLoading(false);
        }
    }

    return(
        <>
            <ToastContainer />
            <div className="SignUp_SignIn_Form_Sect">
                <div className="SignUp_SignIn_Form_SectBG"></div>
                <div className="container">
                    <div className="SignUp_SignIn_Form_Content">
                        <div className="row">
                            <div className="col-md-4">
                                <Link className="banner-btn wow fadeInUp black-shape" to="/">Back</Link>
                            </div>
                            <div className="col-md-4"></div>
                            {/*<div className="col-md-4">
                                <ConnectButton/>
                            </div>*/}
                        </div>
                        {/*<Link className="banner-btn wow fadeInUp black-shape" style={{float: 'right'}}>Back</Link>*/}
                        <div className="SignUp_SignIn_Form signInForm">
                            <h2>PreSale</h2>
                            <p>1 BNB = 30000 RISEROCKET</p>
                            {/*<h3>Inter your email address and password to get access your account</h3>*/}
                            <form>
                                <div className="EmailFild">
                                    <span>Enter Amount of BNB</span>
                                    <input type="number" value={amount} onChange={amountChangeHandle} min='0'/>
                                </div>
                                <div className="PasswordFild">
                                    <span>Recevied Token</span>
                                    <input type="number" value={riseRocket}/>
                                </div>
                                {/*<div className="forgetPassBtnSect">
                                    <div className="KYC_TramsAndCondetionSect m-0">
                                        <lable className="container">Remember me
                                            <input type="checkbox" />
                                            <span className="checkmark"></span>
                                        </lable>
                                    </div>
                                    <div className="ForgetPass">
                                        <a href="forget-password.html">Forget Password ?</a>
                                    </div>
                                </div>*/}

                                <Button
                                    variant="primary"
                                    disabled={isLoading}
                                    onClick={!isLoading ? handleBuyToken : null}
                                >
                                    {isLoading ? 'Loading…' : 'Buy now'}
                                </Button>

                                {/*<a href="index.html">
                                    <div className="project-btn-area text-center black-shape-bigCustom">
                                        <div className="btn_custom" onClick={handleBuyToken}>Buy now</div>
                                        <span className="hover-shape1"></span>
                                        <span className="hover-shape2"></span>
                                        <span className="hover-shape3"></span>
                                    </div>
                                </a>*/}
                            </form>
                            {/*<h4>Don’t have an account ? <a href="signin.html">Sign up now !</a></h4>*/}
                        </div>
                        <div className="singUpformShadow"></div>
                    </div>
                </div>
            </div>
        </>
    )
}