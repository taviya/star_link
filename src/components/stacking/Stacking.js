import React from "react";
import { ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Stacking() {

    return(
        <>
            <ToastContainer />

            <div className="gamfi-project-section HomeV4_OurProject_Sect main-project-area pb-0">
                <div className="container">
                    <div className="sec-inner align-items-center d-flex justify-content-between mb-30">
                        <div className="sec-heading">
                            <div className="sub-inner mb-15">
                                <span className="sub-title">EXPLORE</span>
                                <img className="heading-left-image" src="assets/images/icons/steps.png" alt="" />
                            </div>
                            <h2 className="title">OUR PROJECTS</h2>
                        </div>
                        <div className="gamfi-btn-area ProjectV2_TabBtns">
                            <ul>
                                <li className="ProjectV2_tablinks m-0 active" onclick="openProject(event, 'ProectV2_OnGoing')" id="OpenProject">
                                    <button className="readon white-btn black-shape">
                                        <span className="btn-text">UPCOMING</span>
                                        <span className="hover-shape1"></span>
                                        <span className="hover-shape2"></span>
                                        <span className="hover-shape3"></span>
                                    </button>
                                </li>

                                <li className="ProjectV2_tablinks" onclick="openProject(event, 'ProectV2_ENDED')">
                                    <button className="readon white-btn black-shape">
                                        <span className="btn-text">COMPLETE</span>
                                        <span className="hover-shape1"></span>
                                        <span className="hover-shape2"></span>
                                        <span className="hover-shape3"></span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="ProjectV2_TabContentSect">
                        <div id="ProectV2_OnGoing" className="ProjectV2_tabcontent animate_opacity" style={{display: "block"}}>
                            <div className="gamfi-previous-section V4_OurProjectPreviousSect ProjectClasic_PreviousSection pb-40 md-pb-50">
                                <div className="container-fluid">
                                    <div className="row align-items-center">
                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Lockup 1</div>
                                                        <div className="dec2">3 DAYS LEFT</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">APY</div>
                                                        <div className="dec2">3.7%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Daily Rate</div>
                                                        <div className="dec2">0.01%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Balance</div>
                                                        <div className="dec2">0.00</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Earnings</div>
                                                        <div className="dec2">0.000</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc">Remaining Days</div>
                                                        <div className="dec2">-</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">DEPOSIT</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">WITHDRAW</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">EXTEND LOCKUP</button>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="ProjectV2_TabContentSect">
                        <div id="ProectV2_OnGoing" className="ProjectV2_tabcontent animate_opacity" style={{display: "block"}}>
                            <div className="gamfi-previous-section V4_OurProjectPreviousSect ProjectClasic_PreviousSection pb-40 md-pb-50">
                                <div className="container">
                                    <div className="row align-items-center">
                                        {/*<div className="col-md-12 p-0">
                                            <div className="previous-mainmenu mb-15">
                                                <ul className="menu-list">
                                                    <li className="list1 list1Custom">Project name</li>
                                                    <li className="list2 list2Custom">Chain</li>
                                                    <li className="list3 list3Custom">START IN</li>
                                                    <li className="list4 list4Custom">TARget Raise</li>
                                                    <li className="Customlist5 list5Custom">Progress</li>
                                                </ul>
                                            </div>
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="previous-gaming list1Custom">
                                                        <div className="previous-image">
                                                            <img src="assets/images/project/privius-image11.png" alt="Previous-Image" />
                                                        </div>
                                                        <div className="previous-price">
                                                            <h4 className="mb-10">KyberDyne</h4>
                                                            <div className="dsc">PRICE (GAC) = 0.59 BUSD</div>
                                                        </div>
                                                    </div>
                                                    <div className="previous-chaining list2Custom">
                                                        <img src="assets/images/project/previous-image.png" alt="Chain-Image"/>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <span>9 Hours LEFT</span>
                                                    </div>
                                                    <div className="previous-raise list4Custom">
                                                        <span>100,000 BUSD</span>
                                                    </div>
                                                    <div className="previousProgress list5Custom">
                                                        <div className="ProjectClasicCustomProgressSect">
                                                            <div className="ProgressBg">
                                                                <div className="ProgressBar ProgressBar0">

                                                                </div>
                                                            </div>
                                                            <p><span className="counter">0</span>%</p>
                                                        </div>
                                                    </div>

                                                    <span className="border-shadow shadow-1"></span>
                                                    <span className="border-shadow shadow-2"></span>
                                                    <span className="border-shadow shadow-3"></span>
                                                    <span className="border-shadow shadow-4"></span>
                                                    <span className="hover-shape-bg hover_shape1"></span>
                                                    <span className="hover-shape-bg hover_shape2"></span>
                                                    <span className="hover-shape-bg hover_shape3"></span>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="previous-gaming list1Custom">
                                                        <div className="previous-price">
                                                            <div className="dsc">Lockup</div>
                                                            <span>No Lockup</span>
                                                        </div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="previous-price">
                                                            <div className="dsc">Lockup</div>
                                                            <span>No Lockup</span>
                                                        </div>
                                                    </div>
                                                    <div className="previous-raise list4Custom">
                                                        <span>100,000 BUSD</span>
                                                    </div>
                                                    <div className="previousProgress list5Custom">
                                                        <div className="ProjectClasicCustomProgressSect">
                                                            <div className="ProgressBg">
                                                                <div className="ProgressBar ProgressBar0">

                                                                </div>
                                                            </div>
                                                            <p><span className="counter">0</span>%</p>
                                                        </div>
                                                    </div>

                                                    <span className="border-shadow shadow-1"></span>
                                                    <span className="border-shadow shadow-2"></span>
                                                    <span className="border-shadow shadow-3"></span>
                                                    <span className="border-shadow shadow-4"></span>
                                                    <span className="hover-shape-bg hover_shape1"></span>
                                                    <span className="hover-shape-bg hover_shape2"></span>
                                                    <span className="hover-shape-bg hover_shape3"></span>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="previous-gaming list1Custom">
                                                        <div className="previous-image">
                                                            <img src="assets/images/project/privius-image5.png" alt="Previous-Image" />
                                                        </div>
                                                        <div className="previous-price">
                                                            <h4 className="mb-10">Cyber City</h4>
                                                            <div className="dsc">PRICE (GAC) = 0.59 BUSD</div>
                                                        </div>
                                                    </div>
                                                    <div className="previous-chaining list2Custom">
                                                        <img src="assets/images/project/previous-image5.png" alt="Chain-Image" />
                                                    </div>
                                                    <div className="list3Custom">
                                                        <span>3 DAYS LEFT</span>
                                                    </div>
                                                    <div className="previous-raise list4Custom">
                                                        <span>500,000 BUSD</span>
                                                    </div>
                                                    <div className="previousProgress list5Custom">
                                                        <div className="ProjectClasicCustomProgressSect">
                                                            <div className="ProgressBg">
                                                                <div className="ProgressBar ProgressBar0">

                                                                </div>
                                                            </div>
                                                            <p><span className="counter">0</span>%</p>
                                                        </div>
                                                    </div>

                                                    <span className="border-shadow shadow-1"></span>
                                                    <span className="border-shadow shadow-2"></span>
                                                    <span className="border-shadow shadow-3"></span>
                                                    <span className="border-shadow shadow-4"></span>
                                                    <span className="hover-shape-bg hover_shape1"></span>
                                                    <span className="hover-shape-bg hover_shape2"></span>
                                                    <span className="hover-shape-bg hover_shape3"></span>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="previous-gaming list1Custom">
                                                        <div className="previous-image">
                                                            <img src="assets/images/project/privius-image12.png" alt="Previous-Image" />
                                                        </div>
                                                        <div className="previous-price">
                                                            <h4 className="mb-10">Child ClassName</h4>
                                                            <div className="dsc">price (CCC) = 0.04 BUSD</div>
                                                        </div>
                                                    </div>
                                                    <div className="previous-chaining list2Custom">
                                                        <img src="assets/images/project/previous-image.png" alt="Chain-Image" />
                                                    </div>
                                                    <div className="list3Custom">
                                                        <span>5 DAYS LEFT</span>
                                                    </div>
                                                    <div className="previous-raise list4Custom">
                                                        <span>150,000 BUSD</span>
                                                    </div>
                                                    <div className="previousProgress list5Custom">
                                                        <div className="ProjectClasicCustomProgressSect">
                                                            <div className="ProgressBg">
                                                                <div className="ProgressBar ProgressBar0">

                                                                </div>
                                                            </div>
                                                            <p><span className="counter">0</span>%</p>
                                                        </div>
                                                    </div>


                                                    <span className="border-shadow shadow-1"></span>
                                                    <span className="border-shadow shadow-2"></span>
                                                    <span className="border-shadow shadow-3"></span>
                                                    <span className="border-shadow shadow-4"></span>
                                                    <span className="hover-shape-bg hover_shape1"></span>
                                                    <span className="hover-shape-bg hover_shape2"></span>
                                                    <span className="hover-shape-bg hover_shape3"></span>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="previous-gaming list1Custom">
                                                        <div className="previous-image">
                                                            <img src="assets/images/project/privius-image3.png" alt="Previous-Image" />
                                                        </div>
                                                        <div className="previous-price">
                                                            <h4 className="mb-10">Galaxy War</h4>
                                                            <div className="dsc">PRICE (MTS) = 0.33 BUSD</div>
                                                        </div>
                                                    </div>
                                                    <div className="previous-chaining list2Custom">
                                                        <img src="assets/images/project/previous-image3.png" alt="Chain-Image" />
                                                    </div>
                                                    <div className="list3Custom">
                                                        <span>2 DAYS LEFT</span>
                                                    </div>
                                                    <div className="previous-raise list4Custom">
                                                        <span><span>510,000 BUSD</span></span>
                                                    </div>
                                                    <div className="previousProgress list5Custom">
                                                        <div className="ProjectClasicCustomProgressSect">
                                                            <div className="ProgressBg">
                                                                <div className="ProgressBar ProgressBar0">

                                                                </div>
                                                            </div>
                                                            <p><span className="counter">0</span>%</p>
                                                        </div>
                                                    </div>

                                                    <span className="border-shadow shadow-1"></span>
                                                    <span className="border-shadow shadow-2"></span>
                                                    <span className="border-shadow shadow-3"></span>
                                                    <span className="border-shadow shadow-4"></span>
                                                    <span className="hover-shape-bg hover_shape1"></span>
                                                    <span className="hover-shape-bg hover_shape2"></span>
                                                    <span className="hover-shape-bg hover_shape3"></span>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="previous-gaming list1Custom">
                                                        <div className="previous-image">
                                                            <img src="assets/images/project/privius-image2.png" alt="Previous-Image" />
                                                        </div>
                                                        <div className="previous-price">
                                                            <h4 className="mb-10">Thetan Arena</h4>
                                                            <div className="dsc">PRICE (FSC) = 0.89 BUSD</div>
                                                        </div>
                                                    </div>
                                                    <div className="previous-chaining list2Custom">
                                                        <img src="assets/images/project/previous-image2.png" alt="Chain-Image" />
                                                    </div>
                                                    <div className="list3Custom">
                                                        <span>23 days LEFT</span>
                                                    </div>
                                                    <div className="previous-raise list4Custom">
                                                        <span>265,000 BUSD</span>
                                                    </div>
                                                    <div className="previousProgress list5Custom">
                                                        <div className="ProjectClasicCustomProgressSect">
                                                            <div className="ProgressBg">
                                                                <div className="ProgressBar ProgressBar0">

                                                                </div>
                                                            </div>
                                                            <p><span className="counter">0</span>%</p>
                                                        </div>
                                                    </div>
                                                    <span className="border-shadow shadow-1"></span>
                                                    <span className="border-shadow shadow-2"></span>
                                                    <span className="border-shadow shadow-3"></span>
                                                    <span className="border-shadow shadow-4"></span>
                                                    <span className="hover-shape-bg hover_shape1"></span>
                                                    <span className="hover-shape-bg hover_shape2"></span>
                                                    <span className="hover-shape-bg hover_shape3"></span>
                                                </div>
                                            </a>
                                        </div>*/}

                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Lockup</div>
                                                        <div className="dec2">3 DAYS LEFT</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">APY</div>
                                                        <div className="dec2">3.7%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Daily Rate</div>
                                                        <div className="dec2">0.01%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Balance</div>
                                                        <div className="dec2">0.00</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Earnings</div>
                                                        <div className="dec2">0.000</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc">Remaining Days</div>
                                                        <div className="dec2">-</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">DEPOSIT</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">WITHDRAW</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">EXTEND LOCKUP</button>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Lockup</div>
                                                        <div className="dec2">3 Months</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">APY</div>
                                                        <div className="dec2">11.6%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Daily Rate</div>
                                                        <div className="dec2">0.03%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Balance</div>
                                                        <div className="dec2">0.00</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Earnings</div>
                                                        <div className="dec2">0.000</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc">Remaining Days</div>
                                                        <div className="dec2">-</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">DEPOSIT</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">WITHDRAW</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">EXTEND LOCKUP</button>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Lockup</div>
                                                        <div className="dec2">6 Months</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">APY</div>
                                                        <div className="dec2">15.7%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Daily Rate</div>
                                                        <div className="dec2">0.06%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Balance</div>
                                                        <div className="dec2">0.00</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Earnings</div>
                                                        <div className="dec2">0.000</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc">Remaining Days</div>
                                                        <div className="dec2">-</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">DEPOSIT</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">WITHDRAW</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">EXTEND LOCKUP</button>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Lockup</div>
                                                        <div className="dec2">9 Months</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">APY</div>
                                                        <div className="dec2">24.5%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Daily Rate</div>
                                                        <div className="dec2">0.06%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Balance</div>
                                                        <div className="dec2">0.00</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Earnings</div>
                                                        <div className="dec2">0.000</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc">Remaining Days</div>
                                                        <div className="dec2">-</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">DEPOSIT</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">WITHDRAW</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">EXTEND LOCKUP</button>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div className="col-md-12 p-0">
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Lockup</div>
                                                        <div className="dec2">12 Months</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">APY</div>
                                                        <div className="dec2">33.9%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Daily Rate</div>
                                                        <div className="dec2">0.08%</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Balance</div>
                                                        <div className="dec2">0.00</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc1">Current Earnings</div>
                                                        <div className="dec2">0.000</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <div className="dsc">Remaining Days</div>
                                                        <div className="dec2">-</div>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">DEPOSIT</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">WITHDRAW</button>
                                                    </div>
                                                    <div className="list3Custom">
                                                        <button type="button" className="custom_btn">EXTEND LOCKUP</button>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {/*<div id="ProectV2_ENDED" className="ProjectV2_tabcontent animate_opacity" style="display: none;">
                            <div className="gamfi-previous-section V4_OurProjectPreviousSect ProjectClasic_PreviousSection pb-140 md-pb-50">
                                <div className="container">
                                    <div className="row align-items-center">
                                        <div className="col-md-12 p-0">
                                            <div className="previous-mainmenu mb-15">
                                                <ul className="menu-list">
                                                    <li className="list1 list1Custom">Project name</li>
                                                    <li className="list2 list2Custom">Chain</li>
                                                    <li className="list3 list3Custom">ENDS IN</li>
                                                    <li className="list4 list4Custom">TARget Raise</li>
                                                    <li className="Customlist5 list5Custom">Progress</li>
                                                </ul>
                                            </div>
                                            <a href="project-details.html">
                                                <div className="previous-item hover-shape-border hover-shape-inner">
                                                    <div className="previous-gaming list1Custom">
                                                        <div className="previous-image">
                                                            <img src="assets/images/project/privius-image11.png" alt="Previous-Image" />
                                                        </div>
                                                        <div className="previous-price">
                                                            <h4 className="mb-10">KyberDyne</h4>
                                                            <div className="dsc">PRICE (GAC) = 0.59 BUSD</div>
                                                        </div>
                                                    </div>
                                                    <div className="previous-chaining list2Custom">
                                                        <img src="assets/images/project/previous-image.png" alt="Chain-Image" />
                                                    </div>
                                                    <div className="list3Custom">
                                                        <span>5 Hours ago</span>
                                                    </div>
                                                    <div className="previous-raise list4Custom">
                                                        <span>100,000 BUSD</span>
                                                    </div>
                                                    <div className="previousProgress list5Custom">
                                                        <div className="ProjectClasicCustomProgressSect">
                                                            <div className="ProgressBg">
                                                                <div className="ProgressBar ProgressBar82">

                                                                </div>
                                                            </div>
                                                            <p><span className="counter">82</span>%</p>
                                                        </div>
                                                    </div>

                                                    <span className="border-shadow shadow-1"></span>
                                                    <span className="border-shadow shadow-2"></span>
                                                    <span className="border-shadow shadow-3"></span>
                                                    <span className="border-shadow shadow-4"></span>
                                                    <span className="hover-shape-bg hover_shape1"></span>
                                                    <span className="hover-shape-bg hover_shape2"></span>
                                                    <span className="hover-shape-bg hover_shape3"></span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>*/}
                    </div>
                </div>
            </div>
        </>
    )
}
