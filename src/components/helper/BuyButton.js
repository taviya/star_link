import React, { useEffect, useState } from 'react';
import { useWeb3React } from "@web3-react/core";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'
import { presale_address, presale_abi } from '../../hooks/constant';
import { RPC_URLS } from '../../hooks/connectors';
import { Contract } from '@ethersproject/contracts';
import Countdown from "react-countdown";
import { parseUnits } from '@ethersproject/units';
import { JsonRpcProvider } from '@ethersproject/providers';
import Spinner from 'react-bootstrap/Spinner';


export default function BuyButton() {

    const context = useWeb3React();
    const { library, account } = context;

    const [userBuy, setUserBuy] = useState(false);
    const [whitelist_start, setWhitelist_start] = useState(new Date());
    const [public_start, setPublic_start] = useState(new Date());
    const [whitelist_begain, setWhitelist_begain] = useState(false);
    const [public_begain, setPublic_begain] = useState(false);
    const [amount, setAmount] = useState(0);
    const [errmsg, setErrmsg] = useState('');
    const [loading, setLoading] = useState(false);
    const [refresh, setRefresh] = useState(new Date());
    const singer =  new JsonRpcProvider(RPC_URLS['56']);


    const handleBuytoken = async () => {

        try {
            setLoading(true);
            if (account) {
                
                const presale_contract = new Contract(presale_address, presale_abi, singer);
                let whitelistStartTime = await presale_contract.whitelistStartTime();
                whitelistStartTime = parseInt(whitelistStartTime.toString());

                let whitelistEndTime = await presale_contract.whitelistEndTime();
                whitelistEndTime = parseInt(whitelistEndTime.toString());

                let publicStartTime = await presale_contract.publicStartTime();
                publicStartTime = parseInt(publicStartTime.toString());

                let publicEndTime = await presale_contract.publicEndTime();
                publicEndTime = parseInt(publicEndTime.toString());

                let current_time = Math.floor(new Date().getTime() / 1000.0);

                if (isNaN(amount)) {
                    toast.error('(please enter valid amount.)');
                    setLoading(false);
                    return;
                }
                else if (parseFloat(amount) === 0) {
                    toast.error('(please enter valid amount.)');
                    setLoading(false);
                    return;
                }
                else if (parseFloat(amount) < 0.001 || parseFloat(amount) > 0.01) {
                    toast.error('(amount must be between 0.1 BNB and 3 BNB)');
                    setLoading(false);
                    return;
                }
                else if (current_time > whitelistStartTime && current_time < whitelistEndTime) {

                    let whitelistedAddress = await presale_contract.whitelistedAddress(account);
                    if (whitelistedAddress) {
                        let tx = await presale_contract.investwhitelistBNB(account, { 'from': account, 'value': parseUnits(amount).toString() });
                        let response = await tx.wait();
                        if (response) {
                            if (response.status === 1) {
                                toast.success('success ! Your Last Transaction is Successfull.');
                                setLoading(false);
                            }
                            else if (response.status === 0) {
                                toast.error('error ! Your Last Transaction is Failed.');
                                setLoading(false);
                            }
                            else {
                                toast.error('error ! something went wrong.');
                                setLoading(false);
                            }
                        }
                        else {
                            toast.error('error ! something went wrong.');
                            setLoading(false);
                        }

                    }
                    else {
                        toast.error('Opps ! Your adddress is not whitelisted!');
                        setLoading(false);
                    }

                }
                else if (current_time > publicStartTime && current_time < publicEndTime) {

                    let tx = await presale_contract.investBNB(account, { 'from': account, 'value': parseUnits(amount).toString() });
                    let response = await tx.wait();
                    if (response) {
                        if (response.status === 1) {
                            toast.success('success ! Your Last Transaction is Successfull.');
                            setLoading(false);
                        }
                        else if (response.status === 0) {
                            toast.error('error ! Your Last Transaction is Failed.');
                            setLoading(false);
                        }
                        else {
                            toast.error('error ! something went wrong.');
                            setLoading(false);
                        }
                    }
                    else {
                        toast.error('error ! something went wrong.');
                        setLoading(false);
                    }
                }
                else {
                    toast.error('Opps ! Current Sale is not live!');
                    setLoading(false);
                }
            }
            else {
                toast.error(`Please Connect Wallet ! ${whitelist_begain} ${public_begain}`);
                setLoading(false);
            }
        }
        catch (err) {
            typeof err.data !== 'undefined' ? toast.error(err.data.message) : toast.error(err.message)
            // toast.error(typeof err.data.message !== 'undefined' ? err.data.message : err.message  );
            setLoading(false);
        }
    }

    


    useEffect(() => {

        async function getContarctdata() {

            try {
               
                const presale_contract = new Contract(presale_address, presale_abi, singer);
                let whitelistStartTime = await presale_contract.whitelistStartTime();
                whitelistStartTime = parseInt(whitelistStartTime.toString());

                let whitelistEndTime = await presale_contract.whitelistEndTime();
                whitelistEndTime = parseInt(whitelistEndTime.toString());

                let publicStartTime = await presale_contract.publicStartTime();
                publicStartTime = parseInt(publicStartTime.toString());

                let publicEndTime = await presale_contract.publicEndTime();
                publicEndTime = parseInt(publicEndTime.toString());

                let current_time = Math.floor(new Date().getTime() / 1000.0);

                if (current_time > whitelistStartTime && current_time < whitelistEndTime) {
                    setWhitelist_start(whitelistEndTime * 1000);
                    setPublic_start(publicStartTime * 1000);
                    setWhitelist_begain(true);
                    setPublic_begain(false);
                    if (account) {
                        let whitelistedAddress = await presale_contract.whitelistedAddress(account);
                        if (whitelistedAddress) {
                            setUserBuy(false);
                        }
                        else {
                            setUserBuy(true);
                        }
                    }
                }
                else if (current_time > publicStartTime && current_time < publicEndTime) {
                    setUserBuy(false);
                    setPublic_begain(true);
                    setWhitelist_begain(false);
                    setWhitelist_start('0');
                    setPublic_start(publicEndTime * 1000);

                }
                else {
                    if (current_time > publicEndTime) {
                        setPublic_start('0');
                    }
                    else {
                        setPublic_start(publicStartTime * 1000);
                    }

                    if (current_time > whitelistEndTime) {
                        setWhitelist_start('0');
                    }
                    else {
                        setWhitelist_start(whitelistStartTime * 1000);
                    }
                    setPublic_begain(false);
                    setWhitelist_begain(false);
                    setUserBuy(true);
                }
            }
            catch (err) {
                console.log(err.message);
            }
        }
        getContarctdata();
    }, [refresh , account , singer ]);

   


    const publiccountdown = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) {
            return (
                <>
                    <div className="item" data-days="">00</div>
                    <div className="item" data-hours="">00</div>
                    <div className="item" data-minutes="">00</div>
                    <div className="item" data-seconds="">00</div>
                </>
            );
        } else {
            // Render a countdown
            return (
                <>
                    <div className="item" data-days="">{days}</div>
                    <div className="item" data-hours="">{hours}</div>
                    <div className="item" data-minutes="">{minutes}</div>
                    <div className="item" data-seconds="">{seconds}</div>
                </>
            );

        }
    };


    const whitelistcountdown = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) {

            return (
                <>
                    <div className="item" data-days="">00</div>
                    <div className="item" data-hours="">00</div>
                    <div className="item" data-minutes="">00</div>
                    <div className="item" data-seconds="">00</div>
                </>
            );
        } else {
            // Render a countdown
            return (
                <>
                    <div className="item" data-days="">{days}</div>
                    <div className="item" data-hours="">{hours}</div>
                    <div className="item" data-minutes="">{minutes}</div>
                    <div className="item" data-seconds="">{seconds}</div>
                </>
            );

        }
    };


    const whitelistover = () => {
         setRefresh(new Date());

    }

    const publictover = () => {
        setRefresh(new Date());
    }

    const handleChangeAmount = (e) => {

        let value = e.target.value;

        if (isNaN(value)) {
            setErrmsg('(please enter valid amount.)');
        }
        else if (parseFloat(value) === 0) {
            setErrmsg('(please enter valid amount.)');
        }
        else if (parseFloat(value) < 0.001 || parseFloat(value) > 0.01) {
            setErrmsg('(amount must be between 0.1 BNB and 3 BNB)');
        }
        else {
            setErrmsg('');
        }
        setAmount(value);
    }


    return (
        <>
            <ToastContainer />
            <div className="container">
                <div className="row">
                    <div className="col-xl-4 mb-3">
                        <div className="card">
                            <div className="card-body">
                                <div className="mb-2 text-center" style={{ "color": "#fff" }}>
                                    Whitelist Buy Start In
                                </div>
                                <div className="number-list">
                                    {whitelist_start && <Countdown key={Math.floor((Math.random() * 10) + 1)} onComplete={whitelistover} date={whitelist_start} renderer={whitelistcountdown} />}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-4">
                        <div className="card">
                            <div className="card-body">
                                <div className="mb-2 text-center" style={{ "color": "#fff" }}>
                                    Public Buy Start In
                                </div>
                                <div className="number-list">
                                    {public_start && <Countdown key={Math.floor((Math.random() * 10) + 1)} onComplete={publictover} date={public_start} renderer={publiccountdown} />}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-4">

                        <div className="input-group input-last">
                            <input type="text" className="mb-0 mt-0 form-control-sm" value={amount} onChange={(e) => handleChangeAmount(e)} placeholder="Enter Amount" />
                            <div className="input-group-append">
                                <span className="input-group-text">BNB</span>
                            </div>
                            <span className='text-danger mt-0'><small>{errmsg}</small></span>
                        </div>

                        <button className="btn btn-buy mt-1" disabled={userBuy} onClick={handleBuytoken}>
                            {loading && <Spinner animation="border" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </Spinner>
                            }
                            {!loading &&
                                <>
                                    Buy Now
                                </>}
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}
