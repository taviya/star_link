import { Contract } from '@ethersproject/contracts';
import { JsonRpcProvider } from '@ethersproject/providers';
import { presale_address, presale_abi } from '../../hooks/constant';
import { RPC_URLS } from '../../hooks/connectors';


export let singer =  new JsonRpcProvider(RPC_URLS['97']);

export async function getContract (library = null) {
    try{
        singer = library ? library.getSigner() : singer;
        let contract = new Contract(presale_address, presale_abi);
        return contract.connect(singer);
    }
    catch(err){
        console.log(err.message);
        return false;
    }
    
}

export async function getTokenCount(amount = 0){
    try{
        let contract = await getContract();
        let price = await contract.tokenPerBNB();
        return parseFloat(price.toString() * amount);
    }
    catch(err){
        console.log(err.message);
        return false;
    }     
}


export async function getprivatesaleWhitelisttime(){
    try{
        let contract = await getContract();
        if(contract){
            let wlstarttime = await contract.privatesalewhitelistStartTime();
            let wlendtime = await contract.privatesalewhitelistEndTime();
            wlstarttime = parseInt(wlstarttime.toString() * 1000); 
            wlendtime = parseInt(wlendtime.toString() * 1000);
            return { wlstarttime , wlendtime }
        }
        else{
            return false;
        }
    }
    catch(err){
        console.log(err.message);
        return false;
    }
}

export async function getprivatesalePublictime(){
    try{
        let contract = await getContract();
        if(contract){
            
            let publicstarttime = await contract.privatepublicStartTime();
            let publicendtime = await contract.privatepublicEndTime();
            publicstarttime = parseInt(publicstarttime.toString() * 1000); 
            publicendtime = parseInt(publicendtime.toString() * 1000); 
            return { publicstarttime , publicendtime }
        }
        else{
            return false;
        }
    }
    catch(err){
        console.log(err.message);
        return false;
    }

}

export async function getprivatesaleMaxInvestment(){
    try{
        let contract = await getContract();
        let max = await contract.privatesalemaxInvestment();
        return parseFloat(max.toString() / Math.pow(10,18));
    }
    catch(err){
        console.log(err.message);
        return 0;
    }    
}

export async function getprivatesaleMinInvestment()
{
    try{
        let contract = await getContract();
        let min = await contract.privatesaleminInvestment();
        return parseFloat(min.toString() / Math.pow(10,18));
    }
    catch(err){
        console.log(err.message);
        return 0;
    }    
}

//presale Functions
export async function getpresaleWhitelisttime(){
    try{
        let contract = await getContract();
        if(contract){
            let wlstarttime = await contract.presalewhitelistStartTime();
            let wlendtime = await contract.presalewhitelistEndTime();
            wlstarttime = parseInt(wlstarttime.toString() * 1000); 
            wlendtime = parseInt(wlendtime.toString() * 1000);
            return { wlstarttime , wlendtime }
        }
        else{
            return false;
        }
    }
    catch(err){
        console.log(err.message);
        return false;
    }
}

export async function getpresalePublictime(){
    try{
        let contract = await getContract();
        if(contract){
            
            let publicstarttime = await contract.presalepublicStartTime();
            let publicendtime = await contract.presalepublicEndTime();
            publicstarttime = parseInt(publicstarttime.toString() * 1000); 
            publicendtime = parseInt(publicendtime.toString() * 1000); 
            return { publicstarttime , publicendtime }
        }
        else{
            return false;
        }
    }
    catch(err){
        console.log(err.message);
        return false;
    }

}

export async function getpresaleMaxInvestment(){
    try{
        let contract = await getContract();
        let max = await contract.maxInvestment();
        return parseFloat(max.toString() / Math.pow(10,18));
    }
    catch(err){
        console.log(err.message);
        return 0;
    }    
}

export async function getpresaleMinInvestment()
{
    try{
        let contract = await getContract();
        let min = await contract.minInvestment();
        return parseFloat(min.toString() / Math.pow(10,18));
    }
    catch(err){
        console.log(err.message);
        return 0;
    }    
}


export async function getCurrentRound()
{
    try{
        let contract = await getContract();
        let round = await contract.round();
        return parseInt(round.toString());
    }
    catch(err){
        console.log(err.message);
        return false;
    }    
}


export async function getisClaimStarted(){
   try{
        let contract = await getContract();
        let claim = await contract.isClaimStarted();
        return claim;
    }
    catch(err){
        console.log(err.message);
        return false;
    }   
} 
