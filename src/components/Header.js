import React from "react";
import ConnectButton from "./helper/ConnectButton";

export default function Header() {
    return (
        <>
            <section className="loader_first">
            <div className="circular-spinner"></div>
            </section>
            <header id="gamfi-header" className="gamfi-header-section transparent-header">
                <div className="menu-area menu-sticky">
                    <div className="container">
                        <div className="heaader-inner-area d-flex justify-content-between align-items-center">
                            <div className="gamfi-logo-area d-flex justify-content-between align-items-center">
                                <div className="logo">
                                    <a href="index.html"><img src="assets/images/logo.png" alt="logo"/></a>
                                </div>
                            </div>
                            <div className="gamfi-btn-area">
                                <ul>
                                    <li>
                                        <a id="nav-expander" className="nav-expander bar" href="#sec">
                                            <div className="bar">
                                                <span className="dot1"></span>
                                                <span className="dot2"></span>
                                                <span className="dot3"></span>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="buy-token">
                                        <a className="readon black-shape" href="#sec">
                                            <span className="btn-text">Buy Token </span>
                                            <i className="icon-arrow_down"></i>
                                            <span className="hover-shape1"></span>
                                            <span className="hover-shape2"></span>
                                            <span className="hover-shape3"></span>
                                        </a>
                                        <ul>
                                            <li><a href="#sec"><img src="assets/images/icons/pancake.png"
                                                                 alt="pancake"/> PancakeSwap</a></li>
                                            <li><a href="#sec"><img src="assets/images/icons/uniswap.png"
                                                                 alt="uniswap"/> UniSwap</a></li>
                                            <li><a href="#sec"><img src="assets/images/icons/market.png"
                                                                 alt="market"/> CoinMarketCap</a></li>
                                            <li><a href="#sec"><img src="assets/images/icons/gate.png"
                                                                 alt="gate"/> Gate.io</a></li>
                                        </ul>
                                    </li>
                                    {/*<li>*/}
                                        <ConnectButton/>
                                        {/*<button type="button" className="readon white-btn hover-shape"
                                                data-bs-toggle="modal" data-bs-target="#exampleModal"><img
                                            src="assets/images/icons/connect.png" alt="Icon"/>
                                            <span className="btn-text">Connect </span>
                                            <span className="hover-shape1"></span>
                                            <span className="hover-shape2"></span>
                                            <span className="hover-shape3"></span>
                                        </button>*/}
                                    {/*</li>*/}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </>
    )
}