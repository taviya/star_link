export const presale_address = '0xc70B3104024287B8226711E7b29E5A0fd036f5A3';
export const presale_abi = [{
            "inputs": [{
                "internalType": "uint256",
                "name": "_tokenPerBNB",
                "type": "uint256"
            }, {"internalType": "address", "name": "_token", "type": "address"}],
            "stateMutability": "nonpayable",
            "type": "constructor"
        }, {
            "anonymous": false,
            "inputs": [{
                "indexed": true,
                "internalType": "address",
                "name": "previousOwner",
                "type": "address"
            }, {"indexed": true, "internalType": "address", "name": "newOwner", "type": "address"}],
            "name": "OwnershipTransferred",
            "type": "event"
        }, {
            "anonymous": false,
            "inputs": [{
                "indexed": false,
                "internalType": "uint256",
                "name": "_invested",
                "type": "uint256"
            }, {"indexed": false, "internalType": "uint256", "name": "_tokenDistributed", "type": "uint256"}],
            "name": "Returned",
            "type": "event"
        }, {
            "inputs": [{
                "internalType": "address payable",
                "name": "_reciever",
                "type": "address"
            }, {"internalType": "uint256", "name": "_amount", "type": "uint256"}],
            "name": "bnbLiquidity",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [],
            "name": "bnbRaised",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [{"internalType": "address", "name": "_addr", "type": "address"}],
            "name": "getInvestor",
            "outputs": [{
                "components": [{
                    "internalType": "address",
                    "name": "_name",
                    "type": "address"
                }, {"internalType": "uint256", "name": "_tokenStored", "type": "uint256"}, {
                    "internalType": "uint256",
                    "name": "_bnbInvested",
                    "type": "uint256"
                }], "internalType": "struct RISEROCEKTIDO.Investors", "name": "invest", "type": "tuple"
            }],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "getMaxTOKEN",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [{"internalType": "address", "name": "_investor", "type": "address"}],
            "name": "investBNB",
            "outputs": [{"internalType": "bool", "name": "status", "type": "bool"}],
            "stateMutability": "payable",
            "type": "function"
        }, {
            "inputs": [{"internalType": "address", "name": "", "type": "address"}],
            "name": "investors",
            "outputs": [{"internalType": "address", "name": "_name", "type": "address"}, {
                "internalType": "uint256",
                "name": "_tokenStored",
                "type": "uint256"
            }, {"internalType": "uint256", "name": "_bnbInvested", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "maxBNBSupply",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "maxInvestment",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "minInvestment",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "owner",
            "outputs": [{"internalType": "address", "name": "", "type": "address"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "renounceOwnership",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "name": "salesData",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [{"internalType": "uint256", "name": "_dividend", "type": "uint256"}],
            "name": "setDividend",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [{"internalType": "uint256", "name": "_invest", "type": "uint256"}],
            "name": "setMaxBNBInvestment",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [{"internalType": "uint256", "name": "_invest", "type": "uint256"}],
            "name": "setMaxInvest",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [{"internalType": "uint256", "name": "_invest", "type": "uint256"}],
            "name": "setMinInvest",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [{"internalType": "bool", "name": "_start", "type": "bool"}],
            "name": "setStart",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [{"internalType": "contract IERC20", "name": "_tokenset", "type": "address"}],
            "name": "setTokenAddress",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [{"internalType": "uint256", "name": "_amount", "type": "uint256"}],
            "name": "setperTOKEN",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [],
            "name": "token",
            "outputs": [{"internalType": "contract IERC20", "name": "", "type": "address"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "tokenPerBNB",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "totalInvestments",
            "outputs": [{"internalType": "int256", "name": "", "type": "int256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "totalInvestor",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "name": "totalInvestors",
            "outputs": [{"internalType": "address", "name": "", "type": "address"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [],
            "name": "totalTOKENDistributed",
            "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
            "stateMutability": "view",
            "type": "function"
        }, {
            "inputs": [{"internalType": "address", "name": "payaddress", "type": "address"}, {
                "internalType": "address",
                "name": "tokenAddress",
                "type": "address"
            }, {"internalType": "uint256", "name": "tokens", "type": "uint256"}],
            "name": "transferAnyERC20Token",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "inputs": [{"internalType": "address", "name": "newOwner", "type": "address"}],
            "name": "transferOwnership",
            "outputs": [],
            "stateMutability": "nonpayable",
            "type": "function"
        }]
export const total_sale = 5;

