import { InjectedConnector } from "@web3-react/injected-connector";
import { WalletConnectConnector } from "@web3-react/walletconnect-connector";
// import { WalletLinkConnector } from "@web3-react/walletlink-connector";
// import { ethers } from "ethers";

export const CHAIN_ID = 97;
export const infura_Id = "84842078b09946638c03157f83405213";

export const getRpcUrl = () => {
  return {
    97: 'https://data-seed-prebsc-2-s3.binance.org:8545/',
    56: 'https://bsc-dataseed.binance.org/'
  }[CHAIN_ID]
}

export const RPC_URLS = {
  56: "https://bsc-dataseed.binance.org/",
  97: "https://data-seed-prebsc-2-s3.binance.org:8545/"
};

export const injected = new InjectedConnector({
  supportedChainIds: [1, 2, 4, 56, 97, 31337, 43114, 250, 137, 25, 42161]
})

export const walletconnect = new WalletConnectConnector({
  rpc: RPC_URLS,
  qrcode: true,
  infuraId: infura_Id,
});

// export const coinbaseWallet = new WalletLinkConnector({
//
//   url: `https://mainnet.infura.io/v3/84842078b09946638c03157f83405213`,
//   appName: "Blockstar App",
//  supportedChainIds: [1, 2, 4, 56, 97, 31337, 43114, 250, 137, 25, 42161],
//
//  });


export const POLLING_INTERVAL =  15000;

export const trimAddress = (addr) => {
  return `${addr.substring(0, 6) }...${addr.substring(addr.length - 4)}`;
}
