import Home from "./components/presale/Home";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Presale from "./components/presale/Presale";
import * as contractData from "./components/helper/contractData";
import {useEffect, useState} from "react";
import {total_sale} from "./hooks/constant";
import {Web3ReactProvider} from "@web3-react/core";
import {POLLING_INTERVAL} from "./hooks/connectors";
import {Web3Provider} from "@ethersproject/providers";
import Header from "./components/Header";
import Stacking from "./components/stacking/Stacking";


function getLibrary(provider) {
    const library = new Web3Provider(provider);
    library.pollingInterval = POLLING_INTERVAL;
    return library;
}

function App() {

    const [preSalePercentage, setProgressBar] = useState(0);

    useEffect(() => {
        async function tokenInfo() {
            let contract = await contractData.getContract();

            let bnbRaised = await contract.bnbRaised();
            bnbRaised = bnbRaised.toString() / Math.pow(10, 18);

            let percent = ((bnbRaised * 100) / total_sale);
            setProgressBar(percent);
        }

        tokenInfo();
    });

    return (
        <>
            <div className="HomeV4Bg">

            </div>
            <div className="homeV4Bodycontent">
                <canvas id="canvas" className="home4FireFlyBG"></canvas>

                <div className="home_v4_body_items">

                    <Web3ReactProvider getLibrary={getLibrary}>
                        <Header/>
                        <BrowserRouter>
                            <div>
                                <Routes>
                                    <Route exact path='/'
                                           element={< Home preSalePercentage={preSalePercentage}/>}></Route>
                                    <Route exact path='/presale' element={< Presale/>}></Route>
                                    <Route exact path='/stacking' element={< Stacking/>}></Route>
                                </Routes>
                            </div>
                        </BrowserRouter>
                    </Web3ReactProvider>
                </div>
            </div>

            {/*<ConnectOptionModal/>*/}
        </>
    );
}

export default App;
